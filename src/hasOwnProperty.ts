/**
 * Check to see if an object has a property, typescript cast as object type if it does.
 * @param {any} obj Object to test
 * @param {String} prop Property to check for
 * @returns {Boolean} True if found, false if not
 */
export function hasOwnProperty<
	X extends {} | undefined | string | number | null | any[],
	Y extends PropertyKey
>(obj: X, prop: Y): obj is X & Record<Y, unknown> {
	return !!obj && obj.hasOwnProperty(prop)
}
export default hasOwnProperty
