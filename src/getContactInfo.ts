import getPhoneEmailUrl from "./getPhoneEmailUrl"
import { Company, Contact } from "./types/mtDataTypes"

/**
 * Builds a simple array of phone, email, and URL info.
 * @param  {Object}	libs	Dependencies
 * @param  {Object}	item	Item to be extracted
 * @param  {Func}	t		Translation
 * @return {Array} Array of objects with type and url added to the contact method
 */
export function getContactInfo(
	item: Contact | Company,
	t: (arg: string) => string = (x) => x
) {
	let contactMethods = item.phoneXml ? getPhoneEmailUrl(item) : false

	return contactMethods
		? [
				...contactMethods.phone.map((contact) => ({
					...contact,
					href: `tel:${contact.phone}`,
					type: t("phone"),
				})),
				...contactMethods.email.map((contact) => ({
					...contact,
					href: contact.email && `mailto:${contact.email}`,
					type: t("email"),
				})),
				...contactMethods.url.map((contact) => ({
					...contact,
					href:
						contact.url &&
						(!/^https?:\/\//i.test(contact.url)
							? `https://${contact.url}`
							: contact.url),
					type: t("url"),
				})),
		  ]
		: []
}
export default getContactInfo
