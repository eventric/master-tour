/**
 * String compare function for use in sorting arrays
 *
 * @param {string} a first string to compare
 * @param {string} b second string to compare
 * @returns {number} 1 if b is greater, -1 if a is greater, 0 if equal
 * @example
 * import { compareStrings } from "@eventric/mastertour"
 *
 * // -1
 * const formattedAddress = compareStrings("tango", "bravo");
 * // 0
 * const formattedAddress = compareStrings("racecar", "RACECAR");
 *
 */
export function compareStrings(a: string = "", b: string = ""): number {
	a = a.toString().toUpperCase()
	b = b.toString().toUpperCase()
	return a.localeCompare(b, undefined, {
		ignorePunctuation: false,
		numeric: true,
	})
}
export default compareStrings
