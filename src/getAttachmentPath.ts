import { Attachment } from "./types/mtDataTypes"

/**
 * getAttachmentPath will generate a standardized path for a file based on the organizationId, parentVo/id, and it's subtype.
 * @param {Attachment} attachment Master Tour Attachment record
 * @param {object} options
 * @param {string} options.sep Directory separator, defaults to "/"
 * @param {boolean} options.includeFilename Whether to include the filename in the path output, defaults to true
 * @returns {string} Standardized path for file
 * @example
 * import { getAttachmentPath } from "@eventric/mastertour"
 *
 * const attachment = {
 * 	organizationId: "org12345",
 * 	parentVo: "day",
 * 	parentId: "day12345",
 * 	subtype: "attachment",
 * 	filename: "tech-pack.pdf"
 * };
 *
 * // "org12345/day-day12345/attachment/tech-pack.pdf"
 * const pathToFile = getAttachmentPath(attachment);
 *
 */
export function getAttachmentPath(
	attachment: Attachment,
	{
		sep = "/",
		includeFilename = true,
	}: { sep?: string; includeFilename?: boolean } = {}
) {
	const attachmentPath = [
		attachment.organizationId,
		`${attachment.parentVo}-${attachment.parentId}`,
		attachment.subtype,
	]
	includeFilename && attachmentPath.push(attachment.filename)
	return attachmentPath.join(sep)
}
export default getAttachmentPath
