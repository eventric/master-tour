/**
 * Takes in a rgb color string and determines
 * if color hue is light for dark text or vice verse
 *
 * @param  {object}	options
 * @param  {string} options.incomingString String to check
 * @param  {number} [options.initialsCount] Max number of initials
 * @returns {string} capitalized initials of passed string
 * @example
 * import { initials } from "@eventric/mastertour"
 *
 * const data = { incomingString: "Weird Al Yankovich" };
 *
 * // "WAY"
 * const truncatedData = initials(data);
 *
 */

export function initials({
	incomingString,
	initialsCount = 3,
}: {
	incomingString: string
	initialsCount?: number
}): string {
	let abbrev = ""
	// helper func, loops through a string and returns a new string of chars that match against a regexp
	const getMatchingChars = (testString: string, regex: RegExp) => {
		let matchingChars = ""
		for (let i = 0; i < testString.length; i++) {
			if (testString[i].match(regex)) {
				matchingChars += testString[i]
			}
		}
		return matchingChars
	}
	if (incomingString) {
		// remove special characters except &
		const incomingStringNoSpecialChars = incomingString
			.replace(/[^\w\s(&)]+/g, "")
			.trim()
		// split words from incomingString into separate array values
		const incomingStringSplitted = incomingStringNoSpecialChars.split(" ")
		// regex to find numbers/capitalized letters/ampersands in each string
		const matchRegex = /[A-Z0-9(&)]/
		let firstChar = ""
		// if the first char is a special char, set it equal to firstChar. Ignore letters, numbers, whitespace chars & quotation marks
		if (incomingString[0].match(/[^\w\s("')]/)) {
			firstChar = incomingString[0]
		}
		// if there is more than 1 word in the incomingString
		if (incomingStringSplitted.length > 1) {
			incomingStringSplitted.forEach((val) => {
				// check the first letter of each val for caps/numbers/&
				if (val[0] && val[0].match(matchRegex)) {
					abbrev += getMatchingChars(val, matchRegex)
					// if first letter isn't cap/num/&, abbrev = first char of each val in array
				} else if (val[0]) {
					abbrev += val[0]
				}
			})
			// if there is only 1 word in the incomingString
		} else if (incomingStringSplitted[0]) {
			// check if a string has any caps/numbers/&
			if (incomingStringNoSpecialChars.match(matchRegex)) {
				abbrev = incomingStringNoSpecialChars[0]
				const remaining = incomingStringNoSpecialChars.slice(1)
				abbrev += getMatchingChars(remaining, matchRegex)
			} else {
				// incomingString is only 1 word without chars that match the regex, so set it equal to abbrev
				abbrev = incomingStringNoSpecialChars
			}
		}
		// concat the first char at the front of the final abbrev
		abbrev = firstChar + abbrev
	}
	return abbrev.substr(0, initialsCount).toUpperCase()
}
export default initials
