import moment from "moment-timezone"
/**
 * Check to see if a timezone is valid with a predefined list of invalid options, and others checked from moment-timezone's database
 *
 * @param {string} zone TimeZone value to check
 * @returns {boolean} true if valid, false if not
 * @example
 * import { isValidTimeZone } from "@eventric/mastertour"
 *
 * // true
 * const valid = isValidTimeZone("America/Chicago");
 * // false
 * const valid = isValidTimeZone("glenn");
 *
 */
export function isValidTimeZone(zone: string) {
	const invalidTimeZone = [
		"cet",
		"eet",
		"est",
		"gb",
		"hst",
		"met",
		"mst",
		"nz",
		"uct",
		"wet",
	]
	if (
		zone &&
		typeof zone === "string" &&
		invalidTimeZone.indexOf(zone.toLowerCase()) > -1
	) {
		return false
	}
	return !!moment.tz.zone(zone)
}
export default isValidTimeZone
