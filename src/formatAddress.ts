/**
 * Formats an address as "adressLine1, city, state zip country"
 *
 * @param {object} address
 * @param {string} address.addressLine1 first address
 * @param {string} address.city city
 * @param {string} address.country country
 * @param {string} address.state state
 * @param {string} address.zip zip
 * @param {string} address.zipcode zipcode
 * @param {object} options
 * @param {function} options.translate Translate function, if included translates country value if it is only 2 letters long
 * @param {string} options.translatePrefix String to prefix the country value with if needed
 * @returns {string} Standardized address
 * @example
 * import { formatAddress } from "@eventric/mastertour"
 *
 * const data = {
 * 	addressLine1: "123 Main st",
 * 	city: "MockCity",
 * 	country: "MockCountry"
 * 	state: "MockState",
 * 	zip: "MockZip",
 * };
 *
 * // "123 Main st, MockCity, MockState, MockZip MockCountry"
 * const formattedAddress = formatAddress(data);
 *
 */
export function formatAddress(
	{
		addressLine1,
		city,
		state,
		zip,
		zipcode,
		country,
	}: {
		addressLine1?: string
		city?: string
		state?: string
		zip?: string
		zipcode?: string
		country?: string
	},
	{
		translate,
		translatePrefix = "",
	}: {
		translate?: (arg: string) => string
		translatePrefix?: string
	} = {}
) {
	let address = ""
	if (addressLine1) {
		address += addressLine1
	}
	if (addressLine1 && (city || state || zip || country)) {
		address += ", "
	}
	if (city) {
		address += city
	}
	if (city && (state || zip || country)) {
		address += ", "
	}
	if (state) {
		address += `${state} `
	}
	if (zip || zipcode) {
		address += `${zip || zipcode} `
	}
	if (country) {
		address +=
			country.length === 2 && translate
				? translate(`${translatePrefix}${country}`)
				: country
	}

	return address.trim()
}

export default formatAddress
