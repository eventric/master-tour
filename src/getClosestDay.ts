import moment from "moment"
import { Day } from "./types/mtDataTypes"

/**
 * Returns the closest day record to the current date
 * @param days Array of Master Tour Day objects
 * @returns {Day | Boolean} Master Tour Day object or false if no day found
 */
export function getClosestDay(days: Day[]) {
	// Return false if there are no days in the array
	if (!days.length) {
		return false
	}
	const now = new Date()
	const today = days.filter((day) => moment.utc(day.dayDate).isSame(now, "day"))
	const futureDays = days.filter((day) =>
		moment.utc(day.dayDate).isAfter(now, "day")
	)
	// If there is an event today, return that
	return today.length
		? today[0]
		: // Otherwise if there are future days, return the first one
		futureDays.length
		? futureDays[0]
		: // Otherwise return the last element in the array since there are no future dates / dates today
		  days[days.length - 1]
}
export default getClosestDay
