import {
	EmailData,
	PhoneData,
	UrlData,
	AtLeast,
	PhoneXML,
} from "./types/mtDataTypes"
import hasOwnProperty from "./hasOwnProperty"
type PhoneEmailUrl = {
	[key: string]: EmailData[] | PhoneData[] | UrlData[]
	email: EmailData[]
	phone: PhoneData[]
	url: UrlData[]
}
/**
 * Extract the contact records in an easier to use format from a Company or Contact record
 * @param {Object} item Object with a phoneXml field
 * @returns {Object} Object with arrays of email, phone, and url records.
 */
export function getPhoneEmailUrl(
	item: AtLeast<{ phoneXml: PhoneXML }, "phoneXml">
) {
	const phoneEmailUrl: PhoneEmailUrl = {
		email: [],
		phone: [],
		url: [],
	}
	if (item && hasOwnProperty(item.phoneXml, "dataStore")) {
		const records = item.phoneXml.dataStore.contact
		if (Array.isArray(records)) {
			records.forEach((record) => {
				if (hasOwnProperty(record, "email")) {
					phoneEmailUrl.email.push(record)
				}
				if (hasOwnProperty(record, "phone")) {
					phoneEmailUrl.phone.push(record)
				}
				if (hasOwnProperty(record, "url")) {
					phoneEmailUrl.url.push(record)
				}
			})
		} else if (records) {
			if (hasOwnProperty(records, "email")) {
				phoneEmailUrl.email.push(records)
			}
			if (hasOwnProperty(records, "phone")) {
				phoneEmailUrl.phone.push(records)
			}
			if (hasOwnProperty(records, "url")) {
				phoneEmailUrl.url.push(records)
			}
		}
	}
	return phoneEmailUrl
}
export default getPhoneEmailUrl
