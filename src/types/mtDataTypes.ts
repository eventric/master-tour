export interface MasterTourData {
	[key: string]: any
	createdBy: string
	createdDate: string | number | Date
	id: string
	isDeleted: number | boolean
	isOrganizationVO?: boolean
	modifiedBy: string
	modifiedDate: string | number | Date
	syncChecksum: string | boolean
	syncDate: string | number | Date
	syncId: number
	voType?: string
}

export interface Day extends MasterTourData {
	tourId?: string
	name?: string
	dayDate?: Date
	dayType?: string
	generalNotes?: string
	travelNotes?: string
	hotelNotes?: string
	city?: string
	state?: string
	zipcode?: string
	country?: string
	latLong?: string
	googleMapQuery?: string
	timeZone?: string
	latitude?: number
	longitude?: number
}
export interface DayEvent extends MasterTourData {
	id: string
	dayId: string
	promoterId?: string
	venueId?: string
	name: string
	guestListAllotmentData:
		| {
				allotments: {
					enforcementLevel?: string
					enforcementType?: string
					ident: string
					limit: number
				}[]
				isLocked: boolean
				cutoffTime?: string
		  }
		| { name: string; quantity: number }[]
	localCrewData: {
		costAtSettlement?: number
		costAtSettlementCurrencyValue?: string
		crewComments?: string
		laborCall?: Record<any, any>[]
		localUnion?: string
		minimumIn?: number
		minimumOut?: number
		penalties?: string
	}
}

export interface ScheduleItem extends MasterTourData {
	tourId: string
	parentDayId: string
	parentDayEventId?: string
	scheduleItemTypeId: string
	customFieldsXml: MasterTourXMLData
	assignToXml: MasterTourXMLData
	calendarName: string
	tag?: string
	title: string
	details: string
	isConfirmed: number
	isComplete: number
	startDatetime: string
	startTimeZone?: string
	endDatetime: string
	endTimeZone?: string
	reminderType?: string
	reminderMessage?: string
	reminderDatetime?: string
	timePriority?: string
}

export interface Attachment extends MasterTourData {
	organizationId: string
	filename: string
	filesize: number
	metadata: object
	mimetype: string
	parentVo: string
	parentId: string
	subtype: string
	isUploaded: boolean | number
}

export type PhoneData = { [key: string]: string; phone: string; title: string }
export type EmailData = { [key: string]: string; email: string; title: string }
export type UrlData = { [key: string]: string; url: string; title: string }
export type PhoneEmailUrlData =
	| PhoneData
	| EmailData
	| UrlData
	| (PhoneData | EmailData | UrlData)[]

export type PhoneXML =
	| string
	| {
			dataStore: {
				[key: string]: PhoneEmailUrlData
				contact: PhoneEmailUrlData
			}
	  }

export interface MasterTourLocation extends MasterTourData {
	mtnId?: string
	organizationId?: string
	isLinked?: number
	name?: string
	addressLine1?: string
	addressLine2?: string
	city?: string
	state?: string
	zip?: string | number
	country?: string
	latLong?: string
	googleMapQuery?: string
	timeZone?: string
	phoneXml: PhoneXML
	featuredStatus?: number
	featuredDetails?: string
	latitude?: number
	longitude?: number
}
export interface MasterTourXMLData {
	dataStore: {
		[key: string]: string | number
	}
}
export interface Venue extends MasterTourLocation {
	parentVenueId?: string
	previousNames?: string
	primaryUrl?: string
	primaryEmail?: string
	secondaryEmail?: string
	capacity?: number
	venueTypeId?: string
	ageRequirement?: string
	publicNotes?: string
	privateNotes?: string
	productionXml?: MasterTourXMLData
	facilitiesXml?: MasterTourXMLData
	equipmentXml?: MasterTourXMLData
	localCrewXml?: MasterTourXMLData
	localCrewCallXml?: MasterTourXMLData
	logisticsXml?: MasterTourXMLData
}

export interface Hotel extends MasterTourLocation {
	hotelTypeId?: string
	parentHotelId?: string
	checkinTime?: string
	checkoutTime?: string
	milesToAirport?: number
	facilitiesXml?: MasterTourXMLData
	notes: string
	customFieldsXml?: MasterTourXMLData
}
export interface Company extends MasterTourLocation {
	venueId?: string
	companyId?: string
	assignLabel?: string
	assignPriority?: number
	companyTypeId?: string
	parentCompanyId?: string
	notes?: string
	customFieldsXml?: null
	assignId?: string
}
export interface Contact extends MasterTourData {
	venueId: string
	contactId: string
	assignLabel: string
	assignPriority: number
	parentContactId: string
	mtnId: string
	accountId: number
	organizationId: string
	firstName: string
	middleName: string
	lastName: string
	nickName: string
	stageName: string
	nationality: string
	gender: string
	preferredName: string
	companyName: string
	companyTitle: string
	comapnyDepartment: string
	addressLine1: string
	addressLine2: string
	addressCity: string
	addressState: string
	addressZip: string
	addressCountry: string
	phoneXml: PhoneXML
	emailXml: string
	socialNetworkXml: string
	notes: string
	birthDate: string
	startDate: string
	maritalStatus: string
	spouseName: string
	anniversaryDate: string
	spouseBirthDate: string
	childrenNames: string
	fathersName: string
	mothersName: string
	familyNotes: string
	emergencyContactName: string
	emergencyContactRelationship: string
	emergencyContactPhone: string
	dietRestrictions: string
	dietAllergies: string
	placeOfBirth: string
	driverLicenseNumber: string
	driverLicenseState: string
	socialSecurityNumber: string
	localTaxationNumber: string
	passportXml: string
	travelPreferencesXml: string
	bankAccountXml: string
	creditCardXml: string
	miscData: string
	featuredStatus: number
	featuredDetails: string
	bagTag: string
	travelAlias: string
	assignId: string
}
export type ContactCompanyArray = (Contact | Company)[]

export type AtLeast<T, K extends keyof T> = Partial<T> & Pick<T, K>
