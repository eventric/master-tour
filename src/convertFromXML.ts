import fastXmlParser from "fast-xml-parser"
import he from "he"

/**
 * Convert a Master Tour XML field to a Javascript object
 * @param {string} input XML string to be converted
 * @returns {string|object} If valid XML, it will be a Javascript object, otherwise, the original string
 * @example
 * import { convertFromXML } from "@eventric/mastertour"
 *
 * // {dataStore: { audioPower: "200amp 3-Phase" } }
 * const values = convertFromXML("<dataStore><audioPower>200amp 3-Phase</audioPower></dataStore>");
 * // "some string that isn't XML"
 * const values = convertFromXML("some string that isn't XML");
 *
 */
export const convertFromXML = (input: string) => {
	if (
		/(<([a-zA-Z])[^(><.\s)]+>([\s\S]*)(<([/])[^(><.\s)]+>))|<([a-zA-Z])[^(><.\s)]+\/>/.test(
			input
		)
	) {
		try {
			const modifiedInput = input.trim().replace(/&#xD;/g, "\n")
			return fastXmlParser.parse(modifiedInput, {
				parseTrueNumberOnly: true,
				tagValueProcessor: (value: string) => he.decode(value),
			})
		} catch (err) {
			// Our regex matched something that is not actually XML
			return input
		}
	}
	return input
}

export default convertFromXML
