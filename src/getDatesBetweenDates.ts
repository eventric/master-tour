import moment from "moment"

/**
 * Build an array of date objects beginning with the start date, ending with the end date, and include an array item for each day in-between
 * @param  {Object}	options
 * @param  {Date}   options.startDate Start Date
 * @param  {Date}   options.endDate   End Date
 * @example
 * import { getDatesBetweenDates } from "@eventric/mastertour"
 * // returns [Date(2021, 0, 1), Date(2021, 0, 2), Date(2021, 0, 3)]
 * getDatesBetweenDates({
 * 	startDate: new Date(2021, 0, 1),
 * 	endDate: new Date(2021, 0, 3)
 * })
 * @return {Array}                    Array of date objects
 */
export function getDatesBetweenDates({
	startDate,
	endDate,
}: {
	startDate: Date
	endDate: Date
}) {
	const dates: Date[] = []

	const currDate = moment.utc(startDate).startOf("day").subtract(1, "days")
	const lastDate = moment.utc(endDate).startOf("day")

	while (currDate.add(1, "days").diff(lastDate) <= 0) {
		dates.push(currDate.toDate())
	}

	return dates
}
export default getDatesBetweenDates
