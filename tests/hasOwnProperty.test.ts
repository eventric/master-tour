import hasOwnProperty from "../src/hasOwnProperty"
import { expect } from "chai"

describe("hasOwnProperty", () => {
	it("returns true if property exists", () => {
		expect(hasOwnProperty({ key: "value" }, "key")).to.be.true
	})
	it("returns true if property exists", () => {
		expect(hasOwnProperty({ key: "value" }, "not")).to.be.false
	})
	it("works if a string if passed", () => {
		expect(hasOwnProperty("test", "not")).to.be.false
	})
	it("works if undefined is passed", () => {
		expect(hasOwnProperty(undefined, "test")).to.be.false
	})
	it("works if null is passed", () => {
		expect(hasOwnProperty(null, "test")).to.be.false
	})
})
