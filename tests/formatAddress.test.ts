import { expect } from "chai"
import formatAddress from "../src/formatAddress"

describe("formatAddress", () => {
	let mockData = {
		addressLine1: "123 Main St",
		city: "Oak Forest",
		country: "USA",
		state: "IL",
		zip: "60452",
	}

	it("displays a full address when all elements are included", () => {
		const address = formatAddress(mockData)
		const expected = "123 Main St, Oak Forest, IL 60452 USA"
		expect(address).to.equal(expected)
	})

	it("includes a comma if there is a city and state, zip or country", () => {
		const address = formatAddress({
			...mockData,
			addressLine1: "",
			state: "",
			zip: "",
		})
		const expected = "Oak Forest, USA"
		expect(address).to.equal(expected)
	})

	it("doesnt include a comma if there is no city", () => {
		const address = formatAddress({ ...mockData, addressLine1: "", city: "" })
		const expected = "IL 60452 USA"
		expect(address).to.equal(expected)
	})

	it("shows everything but a country if it's not included", () => {
		const data = {
			addressLine1: "123 Main St",
			city: "Oak Forest",
			state: "IL",
			zip: "60452",
		}
		const address = formatAddress(data)
		const expected = "123 Main St, Oak Forest, IL 60452"
		expect(address).to.equal(expected)
	})

	it("returns an empty string if no data is provided", () => {
		const data = {}
		const address = formatAddress(data)
		expect(address).to.equal("")
	})
	it("returns a zip code if a 'zipcode' is provided instead of 'zip'", () => {
		const data = {
			addressLine1: "123 Main St",
			city: "Oak Forest",
			state: "IL",
			zipcode: "60452",
		}
		const address = formatAddress(data)
		const expected = "123 Main St, Oak Forest, IL 60452"
		expect(address).to.equal(expected)
	})
	it("translates the country if translation details are provided", () => {
		const data = {
			country: "US",
		}
		const address = formatAddress(data, {
			translate: (x) => x,
			translatePrefix: "countries:country",
		})
		const expected = "countries:countryUS"
		expect(address).to.equal(expected)
	})
	it("doesn't translate if the country value is not 2 characters", () => {
		const data = {
			country: "USA",
		}
		const address = formatAddress(data, {
			translate: (x) => x,
			translatePrefix: "countries:country",
		})
		const expected = "USA"

		expect(address).to.equal(expected)
	})
})
