import getPhoneEmailUrl from "../src/getPhoneEmailUrl"
import { expect } from "chai"

describe("getPhoneEmailUrl", () => {
	it("splits apart the items as desired when there are multiple types of records", () => {
		const mockItem = {
			id: "mockId",
			phoneXml: {
				dataStore: {
					contact: [
						{
							label: "mockPhone1",
							phone: "123",
						},
						{
							email: "123@123.com",
							label: "mockEmail1",
						},
						{
							label: "mockUrl1",
							url: "http://123",
						},
					],
				},
			},
		}
		const expected = {
			email: [
				{
					email: "123@123.com",
					label: "mockEmail1",
				},
			],
			phone: [
				{
					label: "mockPhone1",
					phone: "123",
				},
			],
			url: [
				{
					label: "mockUrl1",
					url: "http://123",
				},
			],
		}
		//@ts-ignore
		expect(getPhoneEmailUrl(mockItem)).to.deep.equal(expected)
	})
	it("places the item in the proper object if only one item exists", () => {
		const mockItem = {
			id: "mockId",
			phoneXml: {
				dataStore: {
					contact: {
						label: "mockPhone1",
						phone: "123",
					},
				},
			},
		}
		const expected = {
			email: [],
			phone: [
				{
					label: "mockPhone1",
					phone: "123",
				},
			],
			url: [],
		}
		//@ts-ignore
		expect(getPhoneEmailUrl(mockItem)).to.deep.equal(expected)
	})
	it("returns empty arrays if it cannot find a match", () => {
		const mockItem = {
			id: "mockId",
			phoneXml: {
				dataStore: {},
			},
		}
		const expected = {
			email: [],
			phone: [],
			url: [],
		}
		//@ts-ignore
		expect(getPhoneEmailUrl(mockItem)).to.deep.equal(expected)
	})
	it("gracefully handles if a phoneXml/dataStore is not passed", () => {
		let mockItem = {
			phoneXml: {},
		}
		//@ts-ignore
		expect(() => getPhoneEmailUrl(mockItem)).to.not.throw(Error)
		mockItem = {
			phoneXml: {
				dataStore: {},
			},
		}
		//@ts-ignore
		expect(() => getPhoneEmailUrl(mockItem)).to.not.throw(Error)
	})
	it("includes an object with multiple properties in each of the correct destinations", () => {
		const mockItem = {
			id: "mockId",
			phoneXml: {
				dataStore: {
					contact: [
						{
							phone: "555-555-1212",
							email: "jim@bob.com",
							url: "jimmys.com",
							value: "Venue Contact",
						},
					],
				},
			},
		}
		const expected = {
			email: [
				{
					phone: "555-555-1212",
					email: "jim@bob.com",
					url: "jimmys.com",
					value: "Venue Contact",
				},
			],
			phone: [
				{
					phone: "555-555-1212",
					email: "jim@bob.com",
					url: "jimmys.com",
					value: "Venue Contact",
				},
			],
			url: [
				{
					phone: "555-555-1212",
					email: "jim@bob.com",
					url: "jimmys.com",
					value: "Venue Contact",
				},
			],
		}
		//@ts-ignore
		expect(getPhoneEmailUrl(mockItem)).to.deep.equal(expected)
	})
})
