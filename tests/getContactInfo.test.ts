import getContactInfo from "../src/getContactInfo"
import { expect } from "chai"

describe("getContactInfo", () => {
	let mockItem = {}
	const mockT = () => "mockT output"
	beforeEach(() => {
		mockItem = {
			phoneXml: {
				dataStore: {
					contact: [
						{ email: "mockEmail@eventric.com" },
						{ phone: 1234567890 },
						{ url: "https://www.eventric.com" },
					],
				},
			},
		}
	})
	it("will return phone, email, url objects", () => {
		//@ts-ignore
		const actual = getContactInfo(mockItem, mockT)
		const expected = [
			{ href: "tel:1234567890", phone: 1234567890, type: "mockT output" },
			{
				href: `mailto:mockEmail@eventric.com`,
				type: "mockT output",
				email: "mockEmail@eventric.com",
			},
			{
				href: `https://www.eventric.com`,
				type: "mockT output",
				url: "https://www.eventric.com",
			},
		]
		expect(actual).to.deep.equal(expected)
	})
	it("will return an url with http:// added to href if it is not supplied", () => {
		const expected = `https://eventric.com`
		//@ts-ignore
		mockItem.phoneXml.dataStore.contact[2].url = "eventric.com"
		//@ts-ignore
		const actual = getContactInfo(mockItem, mockT)
		expect(actual[2].href).to.equal(expected)
	})
	it("returns an empty array if item does not have phoneXml", () => {
		mockItem = {}
		//@ts-ignore
		const actual = getContactInfo(mockItem, mockT)
		expect(actual).to.deep.equal([])
	})
})
