import isValidTimeZone from "../src/isValidTimeZone"
import { expect } from "chai"

describe("isValidTimeZone", () => {
	it("returns true for a valid timezone", () => {
		const actual = isValidTimeZone("America/Chicago")
		expect(actual).to.be.true
	})
	it("returns false for an invalid timezone", () => {
		const actual = isValidTimeZone("NZ")
		expect(actual).to.be.false
	})
})
