import { expect } from "chai"

import getDatesBetweenDates from "../src/getDatesBetweenDates"

describe("getDaysBetweenDates", () => {
	it("returns an array of dates for the number dates between them", () => {
		const actual = getDatesBetweenDates({
			endDate: new Date(Date.UTC(2017, 0, 5)),
			startDate: new Date(Date.UTC(2016, 11, 31)),
		})
		expect(actual.length).to.equal(6)
	})
	it("returns the expected date format", () => {
		const actual = getDatesBetweenDates({
			endDate: new Date(Date.UTC(2016, 11, 31)),
			startDate: new Date(Date.UTC(2016, 11, 31)),
		})
		const expected = new Date(Date.UTC(2016, 11, 31))
		expect(actual[0]).to.deep.equal(expected)
	})
	it("returns the day even if the end date is earlier in the day than the start day (but the same date)", () => {
		const actual = getDatesBetweenDates({
			endDate: new Date(Date.UTC(2016, 11, 31, 1, 2, 3)),
			startDate: new Date(Date.UTC(2016, 11, 31, 12, 31, 51)),
		})
		const expected = new Date(Date.UTC(2016, 11, 31))
		expect(actual[0]).to.deep.equal(expected)
	})
})
