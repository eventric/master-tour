import { expect } from "chai"
import getAttachmentPath from "../src/getAttachmentPath"

describe("getAttachmentPath", () => {
	const mockAttachment = {
		createdBy: "",
		createdDate: 0,
		id: "mock1",
		isDeleted: 0,
		modifiedBy: "",
		modifiedDate: 0,
		syncChecksum: "",
		syncDate: 0,
		syncId: 0,
		organizationId: "mockOrgId",
		filename: "mockFilename",
		filesize: 0,
		metadata: {},
		mimetype: "",
		parentVo: "mockParentVo",
		parentId: "mockParentId",
		subtype: "mockSubtype",
		isUploaded: false,
	}
	it("returns the expected string", () => {
		const actual = getAttachmentPath(mockAttachment)
		expect(actual).to.equal(
			"mockOrgId/mockParentVo-mockParentId/mockSubtype/mockFilename"
		)
	})
	it("allows for a custom separator", () => {
		const actual = getAttachmentPath(mockAttachment, { sep: "\\" })
		expect(actual).to.equal(
			"mockOrgId\\mockParentVo-mockParentId\\mockSubtype\\mockFilename"
		)
	})
	it("allows for the filename to not be included", () => {
		const actual = getAttachmentPath(mockAttachment, { includeFilename: false })
		expect(actual).to.equal("mockOrgId/mockParentVo-mockParentId/mockSubtype")
	})
})
