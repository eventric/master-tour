import { expect } from "chai"
import getClosestDay from "../src/getClosestDay"

describe("getClosestDay", () => {
	it("returns an empty string if the passed days array is empty", () => {
		const actual = getClosestDay([])
		expect(actual).to.equal(false)
	})
	it("returns the first item from the array for when it's the only item in the array", () => {
		const mockDays = [
			{
				createdBy: "",
				createdDate: 0,
				id: "mock1",
				isDeleted: 0,
				modifiedBy: "",
				modifiedDate: 0,
				syncChecksum: "",
				syncDate: 0,
				syncId: 0,
				tourId: "mockTourId",
				dayDate: new Date(),
			},
		]
		const actual = getClosestDay(mockDays)
		expect(actual).to.equal(mockDays[0])
	})
	it("returns the first item from the array for when isSame returns false and isAfter returns true", () => {
		const today = new Date()
		const tomorrow = new Date()
		tomorrow.setDate(today.getDate() + 1)
		const mockDays = [
			{
				createdBy: "",
				createdDate: 0,
				id: "mock1",
				isDeleted: 0,
				modifiedBy: "",
				modifiedDate: 0,
				syncChecksum: "",
				syncDate: 0,
				syncId: 0,
				tourId: "mockTourId",
				dayDate: today,
			},
			{
				createdBy: "",
				createdDate: 0,
				id: "mock2",
				isDeleted: 0,
				modifiedBy: "",
				modifiedDate: 0,
				syncChecksum: "",
				syncDate: 0,
				syncId: 0,
				tourId: "mockTourId",
				dayDate: tomorrow,
			},
		]
		const actual = getClosestDay(mockDays)
		expect(actual).to.equal(mockDays[0])
	})
	it("returns the last item from the array for when it's the closest to today", () => {
		const today = new Date()
		const yesterday = new Date()
		yesterday.setDate(today.getDate() - 1)
		const mockDays = [
			{
				createdBy: "",
				createdDate: 0,
				id: "mock1",
				isDeleted: 0,
				modifiedBy: "",
				modifiedDate: 0,
				syncChecksum: "",
				syncDate: 0,
				syncId: 0,
				tourId: "mockTourId",
				dayDate: yesterday,
			},
			{
				createdBy: "",
				createdDate: 0,
				id: "mock2",
				isDeleted: 0,
				modifiedBy: "",
				modifiedDate: 0,
				syncChecksum: "",
				syncDate: 0,
				syncId: 0,
				tourId: "mockTourId",
				dayDate: today,
			},
		]
		const actual = getClosestDay(mockDays)
		expect(actual).to.equal(mockDays[1])
	})
})
