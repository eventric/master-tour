import { expect } from "chai"

import initials from "../src/initials"

describe("initials", () => {
	it("handles an empty string", () => {
		const mockString = "    "
		const actual = initials({incomingString: mockString})
		expect(actual).to.equal("")
	})
	it("will return the capital characters if string passed in is only one word", () => {
		const mockString = "HelloWorld"
		const actual = initials({incomingString: mockString})
		expect(actual).to.equal("HW")
	})
	it("will return 2 characters if only 2 words are passed", () => {
		const mockString = "Hello World"
		const actual = initials({incomingString: mockString})
		expect(actual).to.equal("HW")
	})
	it("will return 5 characters", () => {
		const mockString = "taytayfriday"
		const actual = initials({incomingString: mockString, initialsCount: 5})
		expect(actual).to.equal("TAYTA")
	})
	it("will not return any quotes", () => {
		const mockString = `"hello" 'world' "Now" "Gov'nor"`
		const actual = initials({incomingString: mockString})
		expect(actual).to.equal("HWN")
	})
	it("returns RL from RaeLynn", () => {
		const mockString = "RaeLynn"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("RL")
	})
	it("returns BRC from Billy Rae Cyrus", () => {
		const mockString = "Billy Rae Cyrus"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("BRC")
	})
	it("returns BLA from BLACK EYED PEAS", () => {
		const mockString = "BLACK EYED PEAS"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("BLA")
	})
	it("returns ONE from onerepublic", () => {
		const mockString = "onerepublic"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("ONE")
	})
	it("returns OR from oneRepublic", () => {
		const mockString = "oneRepublic"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("OR")
	})
	it("returns YYY from yeah yeah yeahs", () => {
		const mockString = "yeah yeah yeahs"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("YYY")
	})
	it("returns 311 from 311", () => {
		const mockString = "311"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("311")
	})
	it("returns 311 from 3 1 1", () => {
		const mockString = "3 1 1"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("311")
	})
	it("returns 3DN from 3 Dog Night", () => {
		const mockString = "3 Dog Night"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("3DN")
	})
	it("returns MMD from Michael McDonald", () => {
		const mockString = "Michael McDonald"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("MMD")
	})
	it("returns M&S from Mumford & Sons", () => {
		const mockString = "Mumford & Sons"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("M&S")
	})
	it("returns M&S from mumford&Sons", () => {
		const mockString = "mumford&Sons"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("M&S")
	})
	it("returns *SE from *Special Events - WMN", () => {
		const mockString = "*Special Events --- WMN"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("*SE")
	})
	it("returns SE from Special-$-Events", () => {
		const mockString = "Special-$-Events"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("SE")
	})
	it("returns Chris Janson - WMN", () => {
		const mockString = "Chris Janson - WMN"
		const actual = initials({ incomingString: mockString })
		expect(actual).to.equal("CJW")
	})
})
