import { expect } from "chai"
import compareStrings from "../src/compareStrings"

describe("compareStrings", () => {
	it("doesn't fail if the strings are not passed", () => {
		expect(() => compareStrings()).to.not.throw(Error)
	})
	it("returns 0 when strings are the same", () => {
		expect(compareStrings("a", "a")).to.equal(0)
	})
	it("returns a -1 when the first parameter is greater than the second, regardless of case", () => {
		expect(compareStrings("a", "B")).to.equal(-1)
	})
	it("returns a 1 when the first parameter is less than the second, regardless of case", () => {
		expect(compareStrings("B", "a")).to.equal(1)
	})
})
