import { expect } from "chai"
import convertFromXML from "../src/convertFromXML"

describe("convertFromXML", () => {
	it("converts XML into an object", () => {
		const xmlInput =
			"<datastore><contact><title>Main Number</title><contactName></contactName><phone>616-942-1328</phone></contact></datastore>"
		const actual = convertFromXML(xmlInput)
		expect(actual).to.deep.equal({
			datastore: {
				contact: {
					title: "Main Number",
					contactName: "",
					phone: "616-942-1328",
				},
			},
		})
	})
	it("attempts to convert XML prefixed with a non-permitted characters to an object", () => {
		const actual = convertFromXML(`
<dataStore>
  <stageWings></stageWings>
  <paBays></paBays>
  <orchestraPit></orchestraPit>
  <curtain></curtain>
  <fireCurtain></fireCurtain>
  <steps></steps>
  <trimHeight></trimHeight>
  <stage>Front of House / Monitor and Lighting locations:&#xD;
FOH: Center of dance floor 35&amp;#039; from stage Bike rack barricade provided. Also have blow through barricade for front of stage.&#xD;
House monitor board is on a loft off stage left. There is room to set up visiting monitor desks off stage left on floor beneath loft.&#xD;
Lighting is located upstairs in DJ booth can make room for visiting desks.</stage>
  <stageRisers></stageRisers>
  <deckToGrid></deckToGrid>
  <legs></legs>
  <borders></borders>
  <dimensionsW>27&amp;#039;</dimensionsW>
  <dimensionsD>21&amp;#039;</dimensionsD>
  <dimensionsH>5&amp;#039;</dimensionsH>
  <prosceniumW></prosceniumW>
  <prosceniumH>15&amp;#039;</prosceniumH>
  <prosceniumD></prosceniumD>
  <access>Side door on First Avenue</access>
  <dockType></dockType>
  <deadCase></deadCase>
  <forklifts></forklifts>
  <loadComments>Load in and load out:&#xD;
Flat push from garage to center of dance floor @ 150&amp;#039; Ramp to stage.&#xD;
Load in door dimensions 56 1/2&amp;quot; w x 82 1/2&amp;quot; h&#xD;
In most cases room for storage of empty cases in venue.</loadComments>
  <flySystem></flySystem>
  <lineSets></lineSets>
  <riggingComments></riggingComments>
  <apron></apron>
  <audioPower></audioPower>
  <lightingPower></lightingPower>
  <otherPower></otherPower>
  <shorePower/>
  <showPower></showPower>
  <distToStage></distToStage>
  <powerComments></powerComments>
  <houseLights></houseLights>
  <climateControl></climateControl>
  <fog></fog>
  <pyro></pyro>
  <co2></co2>
  <fohPosition></fohPosition>
  <snakeRun></snakeRun>
  <barricade></barricade>
  <roof></roof>
  <spots></spots>
  <venueSeats></venueSeats>
  <customLabel1></customLabel1><customField1></customField1>
  <customLabel2></customLabel2><customField2></customField2>
  <customLabel3></customLabel3><customField3></customField3>
  <customLabel4></customLabel4><customField4></customField4>
  <customLabel5></customLabel5><customField5></customField5>
<localCrewContacts></localCrewContacts></dataStore>`)
		expect(actual).to.be.an("object")
		expect(actual.dataStore.stage).to
			.equal(`Front of House / Monitor and Lighting locations:

FOH: Center of dance floor 35&#039; from stage Bike rack barricade provided. Also have blow through barricade for front of stage.

House monitor board is on a loft off stage left. There is room to set up visiting monitor desks off stage left on floor beneath loft.

Lighting is located upstairs in DJ booth can make room for visiting desks.`)
	})
	it("attempts to convert XML prefixed with html entities to an object", () => {
		const actual = convertFromXML(`<xml>
<stageWings></stageWings>
<paBays></paBays>
<orchestraPit></orchestraPit>
<curtain></curtain>
<fireCurtain></fireCurtain>
<steps></steps>
<trimHeight></trimHeight>
<apron></apron>
<deckToGrid>54'6</deckToGrid>
<legs></legs>
<borders></borders>
<stageRisers></stageRisers>
<dimensionsW>60'</dimensionsW>
<dimensionsD>40'</dimensionsD>
<dimensionsH>5'</dimensionsH>
<prosceniumW></prosceniumW>
<prosceniumD></prosceniumD>
<prosceniumH></prosceniumH>
<stage>Stage Right staging</stage>
<access>Show load-in is located at the northeast corner of the Arena, approximately 55' from the northeast vomitory and event floor.</access>
<dockType>Docks are covered, and include an area for a tractor-trailer production trucks (54' long trailer) and vehicles.</dockType>
<deadCase>Behind and underneath the stage or back on the dock</deadCase>
<forklifts>Yes, with 5,000 lb. capacity</forklifts>
<loadComments></loadComments>
<flySystem></flySystem>
<lineSets></lineSets>
<riggingComments>Distance from the Arena event-level floor to bottom of steel roof trusses is 54&#226;&#128;&#153;-6&#226;&#128;&#157; clear. The steel framing below this height is limited to the spotlight platforms, the bottom of which are 46&#226;&#128;&#153; clear above the event fl oor. The eight main trusses vary in height from 8&#226;&#128;&#153; at the ends to 24&#226;&#128;&#153; in the center. Specifi c rigging locations, capacities, and configurations are limited to the attached pdf diagram notes.

&#226;&#128;&#162; End Stage curtain rigging system: Single, electrically operated, 80&#226;&#128;&#153; end stage rear curtain masking batten with 2,000 pound lifting/sustaining capacity.
&#226;&#128;&#162; Flexible/portable truss and motor system: 210 linear feet of box truss, 11 rigstar 1 ton motors with control system, full 47&#226;&#128;&#153; tall black velour curtains
&#226;&#128;&#162; Two (2) 200A-3P cam-lock plug in devices at 208v, 3-phase, 5-wire, located on the north and south catwalks (one each) for show power
&#226;&#128;&#162; One (1) telecom quad (4 jacks) at each of the plug-in locations as well as others on the catwalk.

Fall - Arrest System
The Arena has an engineered fall-arrest system accessible from the catwalk. The main cross-stage cables are equipped with &#226;&#128;&#156;hands free&#226;&#128;&#157; capability and the system has been designed for two (2) users per sub span with the maximum number of users limited as four (4) per cable.</riggingComments>
<audioPower></audioPower>
<lightingPower></lightingPower>
<otherPower></otherPower>
<showPower></showPower>
<distToStage></distToStage>
<powerComments>Stage Left: All panels are 3-phase stage panels,
with cam lock-style connections:
&#226;&#128;&#162; Two (2) 400-amp panels 208v/120v
&#226;&#128;&#162; One (1) 200-amp panel 208v/120v
&#226;&#128;&#162; Two (2) telecom quads
Stage Right: All panels are 3-phase stage panels,
with cam lock-type connections:
&#226;&#128;&#162; Two (2) 400-amp panels 208v/120v
&#226;&#128;&#162; One (1) 200-amp panel 208v/120v
&#226;&#128;&#162; Two (2) telecom quads</powerComments>
<houseLights>Controlled from the truss level control room and operations offices</houseLights>
<climateControl></climateControl>
<fog></fog>
<pyro>Pyrotechnics and smoke/fogging effects are allowed with a special permit issued by the Boston Fire Department.</pyro>
<co2></co2>
<fohPosition></fohPosition>
<snakeRun></snakeRun>
<barricade>Mojo-style blow through</barricade>
<roof></roof>
<spots>6 - 1290 LT Lycian long throws</spots>
<venueSeats></venueSeats>
<customLabel1></customLabel1>
<customField1></customField1>
<customLabel2></customLabel2>
<customField2></customField2>
<customLabel3></customLabel3>
<customField3></customField3>
<customLabel4></customLabel4>
<customField4></customField4>
<customLabel5></customLabel5>
<customField5></customField5>
</xml>
`)
		expect(actual).to.be.an("object")
	})
	it("doesn't sent <No Schedule> to the XML conversion", () => {
		const actual = convertFromXML("<No Schedule>")
		const expected = "<No Schedule>"
		expect(actual).to.equal(expected)
	})
})
