<img alt="Master Tour" src="https://img.mysignature.io/p/2/1/8/218f4291-c32c-504a-bad1-6da2fa88f31c.png" width="75">

# Master Tour

A library for interacting programmatically with the [Master Tour](https://www.eventric.com/master-tour-management-software/) platform. This library is under development.

### Usage

```
$ npm i @eventric/mastertour
```

```
import MasterTour from "@eventric/mastertour"
```

### Documentation

[View Documentation](https://eventric.bitbucket.io)
